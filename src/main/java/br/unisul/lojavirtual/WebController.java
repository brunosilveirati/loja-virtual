package br.unisul.lojavirtual;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import io.javalin.BadRequestResponse;
import io.javalin.Context;
import io.javalin.Javalin;
import io.javalin.NotFoundResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.sql2o.Connection;
import org.sql2o.Sql2oException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@RequiredArgsConstructor
public class WebController {

    private static final String ATTR_PEDIDO = "pedido";
    private final LojaVirtual lojaVirtual;

    public void inicializar(Javalin javalin) {
        javalin.get("robots.txt", this::renderizarRobotsTxt);
        javalin.get("", ctx -> ctx.render("templates/disclaimer.twig"));
        javalin.get("obrigado", ctx -> ctx.render("templates/obrigado.twig"));
        javalin.get("catalogo", this::renderizarCatalogo);
        javalin.get("catalogo/:produtoId", this::renderizarProduto);
        javalin.get("checkout", this::renderizarCheckout);
        javalin.post("checkout", this::finalizarCompra);
        javalin.get("carrinho", this::renderizarCarrinho);
        javalin.post("adicionar-carrinho", this::adicionarProduto);
        javalin.post("remover-carrinho", this::removerProduto);
    }

    private Pedido getOuCriarPedido(Context ctx) {
        Pedido pedido = ctx.sessionAttribute(ATTR_PEDIDO);

        if (pedido == null) {
            pedido = new Pedido();
            ctx.sessionAttribute(ATTR_PEDIDO, pedido);

            pedido.putMetdata(Pedido.METADATA_START_TIME, System.currentTimeMillis());
        }
        return pedido;
    }

    private void renderizarRobotsTxt(Context ctx) {
        StringBuilder builder = new StringBuilder();

        builder.append("User-agent: *\n");
        builder.append("Disallow: /");

        ctx.result(builder.toString());
    }

    private void renderizarProduto(Context ctx) {
        String produtoId = ctx.pathParam("produtoId");
        Produto produto = lojaVirtual.getProdutos().get(produtoId);

        if (produto == null) {
            throw new NotFoundResponse("Produto não encontrado");
        }
        Pedido pedido = getOuCriarPedido(ctx);
        Integer productViews = pedido.getOuCriarMetadata(Pedido.METADATA_PRODUCT_VIEWS, k -> 1);
        productViews++;
        pedido.putMetdata(Pedido.METADATA_PRODUCT_VIEWS, productViews);

        HashMap<String, Object> model = new HashMap<>();

        model.put("produto", produto);
        ctx.render("templates/produto-detalhe.twig", model);
    }

    private void renderizarCatalogo(Context ctx) {
        Pedido pedido = getOuCriarPedido(ctx);

        Integer catalogViews = pedido.getOuCriarMetadata(Pedido.METADATA_CATALOG_VIEWS, k -> 1);
        catalogViews++;
        pedido.putMetdata(Pedido.METADATA_CATALOG_VIEWS, catalogViews);

        HashMap<String, Object> model = new HashMap<>();

        model.put("produtos", lojaVirtual.getProdutos());
        model.put("carrinhoVazio", pedido.isEmpty());

        ctx.render("templates/catalogo.twig", model);
    }

    private void renderizarCarrinho(Context ctx) {
        HashMap<String, Object> model = new HashMap<>();
        Pedido pedido = getOuCriarPedido(ctx);

        model.put("carrinho", pedido.getProdutos());

        ctx.render("templates/carrinho.twig", model);
    }

    private void renderizarCheckout(Context ctx) {
        HashMap<String, Object> model = new HashMap<>();
        Pedido pedido = getOuCriarPedido(ctx);

        if (pedido.isEmpty()) {
            ctx.redirect("/catalogo");
            return;
        }

        model.put("carrinho", pedido.getProdutos());
        model.put("valorTotal", pedido.getSubtotal());

        ctx.render("templates/checkout.twig", model);
    }

    private void finalizarCompra(Context ctx) {
        Pedido pedido = getOuCriarPedido(ctx);

        if (pedido.isEmpty()) {
            ctx.redirect("/catalogo");
            return;
        }

        Gson gson = lojaVirtual.getGson();
        JsonObject jsonObject;
        try {
            jsonObject = gson.fromJson(ctx.req.getReader(), JsonObject.class);
        } catch (IOException | JsonParseException e) {
            throw new BadRequestResponse(); // no message error
        }

        long endTimeMillis = System.currentTimeMillis();

        pedido.putMetdata(Pedido.METADATA_USER_AGENT, ctx.header("User-Agent"));
        pedido.putMetdata(Pedido.METADATA_END_TIME, endTimeMillis);

        Long startTimeMillis = pedido.getMetadata(Pedido.METADATA_START_TIME);
        pedido.putMetdata(Pedido.METADATA_TOTAL_TIME, endTimeMillis - startTimeMillis);

        pedido.putMetdata(Pedido.METADATA_FORM_DATA, gson.fromJson(jsonObject.get("formData"), TypeToken.getParameterized(Map.class, String.class, String.class).getType()));
        pedido.putMetdata(Pedido.METADATA_INPUT_FOCUS_TIMES, gson.fromJson(jsonObject.get("inputFocusTimes"), TypeToken.getParameterized(Map.class, String.class, Integer.class).getType()));

        if (lojaVirtual.analisarFraude(pedido)) {
            LOGGER.warn("Possibilidade de fraude detectada no pedido");
            throw new BadRequestResponse("Não foi possível finalizar a compra");
        }

        String data = gson.toJson(pedido);
        LOGGER.info("New purchase={}", data);

        try (Connection connection = lojaVirtual.getSql2o().open()) {
            connection.createQuery("INSERT INTO purchases(data) VALUES (:data)")
                      .addParameter("data", data)
                      .executeUpdate();
        } catch (Sql2oException e) {
            LOGGER.error("Failed to persist purchase to database", e);
            ctx.redirect("/checkout");
            return;
        }

        ctx.sessionAttribute(ATTR_PEDIDO, null);

        ctx.redirect("/obrigado");
    }

    private void removerProduto(Context ctx) {
        String produtoId = ctx.formParam("produtoId");

        if (produtoId == null) {
            throw new BadRequestResponse("O produtoId é obrigatório");
        }
        Produto produto = lojaVirtual.getProdutos().get(produtoId);

        if (produto == null) {
            throw new NotFoundResponse("Produto não encontrado");
        }
        Pedido pedido = getOuCriarPedido(ctx);

        pedido.remover(produto);
    }

    private void adicionarProduto(Context ctx) {
        String produtoId = ctx.formParam("produtoId");

        if (produtoId == null) {
            throw new BadRequestResponse("O produtoId é obrigatório");
        }
        Produto produto = lojaVirtual.getProdutos().get(produtoId);

        if (produto == null) {
            throw new NotFoundResponse("Produto não encontrado");
        }
        Pedido pedido = getOuCriarPedido(ctx);

        pedido.adicionar(produto);
    }
}
