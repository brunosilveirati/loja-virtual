package br.unisul.lojavirtual;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import de.daslaboratorium.machinelearning.classifier.Classifier;
import de.daslaboratorium.machinelearning.classifier.bayes.BayesClassifier;
import io.javalin.Javalin;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.sql2o.Sql2o;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Getter
public class LojaVirtual {

    private static final String CATEGORY_BOT = "bot";
    private static final String CATEGORY_HUMAN = "human";

    private final Classifier<String, String> bayesClassifier = new BayesClassifier<>();
    private final Gson gson = new Gson();
    private final Map<String, Produto> produtos = new HashMap<>();
    private Sql2o sql2o;

    public void inicializar() throws Exception {
        carregarProdutos();
        carregarDadosTreinamento();

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:sqlite:data/database.sqlite");

        sql2o = new Sql2o(new HikariDataSource(config));

        Javalin javalin = Javalin.create()
                                 .enableCaseSensitiveUrls();

        WebController webController = new WebController(this);
        webController.inicializar(javalin);

        javalin.exception(Exception.class, ((e, ctx) -> {
            ctx.status(500);
            LOGGER.error("Exception handler", e);
        }));
        javalin.start();
    }

    private void carregarProdutos() throws IOException {
        try (InputStreamReader reader = new InputStreamReader(getClass().getResourceAsStream("/produtos.json"))) {
            List<Produto> localProdutos = gson.fromJson(reader, TypeToken.getParameterized(ArrayList.class, Produto.class).getType());

            localProdutos.forEach(produto -> produtos.put(produto.getId(), produto));
        }
    }

    private void carregarDadosTreinamento() throws IOException, URISyntaxException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/training_set_bot.txt")))) {
            int count = 0;
            String line;

            while ((line = reader.readLine()) != null) {
                bayesClassifier.learn(CATEGORY_BOT, Arrays.asList(line.split(",")));
                count++;
            }
            LOGGER.info("Classifier treinado com {} entradas para a categoria {}", count, CATEGORY_BOT);
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/training_set_human.txt")))) {
            int count = 0;
            String line;

            while ((line = reader.readLine()) != null) {
                bayesClassifier.learn(CATEGORY_HUMAN, Arrays.asList(line.split(",")));
                count++;
            }
            LOGGER.info("Classifier treinado com {} entradas para a categoria {}", count, CATEGORY_HUMAN);
        }
    }

    public boolean analisarFraude(Pedido pedido) {
        String category = bayesClassifier.classify(extrairFeatures(pedido)).getCategory();

        switch (category) {

            case CATEGORY_BOT:
                return true;
            case CATEGORY_HUMAN:
                return false;
            default:
                return true;
        }
    }

    private List<String> extrairFeatures(Pedido pedido) {
        List<String> features = new ArrayList<>();

        Map<String, String> formData = pedido.getMetadata(Pedido.METADATA_FORM_DATA);
        Map<String, Integer> inputFocusTimes = pedido.getMetadata(Pedido.METADATA_INPUT_FOCUS_TIMES);

        features.add(classificarTempoDeFoco(inputFocusTimes.getOrDefault("firstName", 0)));
        features.add(classificarTempoPorCaractere(inputFocusTimes.getOrDefault("firstName", 1).floatValue() / formData.get("firstName").length()));

        features.add(classificarTempoDeFoco(inputFocusTimes.getOrDefault("lastName", 0)));
        features.add(classificarTempoPorCaractere(inputFocusTimes.getOrDefault("lastName", 1).floatValue() / formData.get("lastName").length()));

        features.add(classificarTempoDeFoco(inputFocusTimes.getOrDefault("email", 0)));
        features.add(classificarTempoPorCaractere(inputFocusTimes.getOrDefault("email", 1).floatValue() / formData.get("email").length()));

        features.add(classificarTempoDeFoco(inputFocusTimes.getOrDefault("zip", 0)));
        features.add(classificarTempoPorCaractere(inputFocusTimes.getOrDefault("zip", 1).floatValue() / formData.get("zip").length()));

        features.add(classificarTempoDeFoco(inputFocusTimes.getOrDefault("address", 0)));
        features.add(classificarTempoPorCaractere(inputFocusTimes.getOrDefault("address", 1).floatValue() / formData.get("address").length()));
        return features;
    }

    private String classificarTempoDeFoco(int value) {

        if (value < 200) {
            return "muito_rapido";
        } else if (value < 1000) {
            return "rapido";
        } else if (value < 5000) {
            return "normal";
        } else if (value < 10000) {
            return "lento";
        } else {
            return "muito_lento";
        }
    }

    private String classificarTempoPorCaractere(float value) {

        if (value < 100) {
            return "muito_rapido";
        } else if (value < 300) {
            return "rapido";
        } else if (value < 800) {
            return "normal";
        } else if (value < 1500) {
            return "lento";
        } else {
            return "muito_lento";
        }
    }
}
