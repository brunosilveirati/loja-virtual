package br.unisul.lojavirtual;

import lombok.Getter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Getter
public class Pedido {

    public static final String METADATA_START_TIME = "startTimeMillis";
    public static final String METADATA_END_TIME = "endTimeMillis";
    public static final String METADATA_TOTAL_TIME = "timeToPurchaseMillis";
    public static final String METADATA_PRODUCT_VIEWS = "productViews";
    public static final String METADATA_CATALOG_VIEWS = "catalogViews";
    public static final String METADATA_USER_AGENT = "userAgent";
    public static final String METADATA_INPUT_FOCUS_TIMES = "inputFocusTimes";
    public static final String METADATA_FORM_DATA = "formData";

    private final List<Produto> produtos = new ArrayList<>();
    private final Map<String, Object> metadata = new HashMap<>();

    public void adicionar(Produto produto) {
        produtos.add(produto);
    }

    public void remover(Produto produto) {
        produtos.remove(produto);
    }

    public BigDecimal getSubtotal() {
        BigDecimal subtotal = BigDecimal.ZERO;

        for (Produto produto : produtos) {
            subtotal = subtotal.add(produto.getPreco());
        }
        return subtotal;
    }

    public boolean isEmpty() {
        return produtos.isEmpty();
    }

    @SuppressWarnings("unchecked")
    public <T> T getMetadata(String key) {
        return (T) metadata.get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T getOuCriarMetadata(String key, Function<String, T> producer) {
        return (T) metadata.computeIfAbsent(key, producer);
    }

    public void putMetdata(String key, Object value) {
        metadata.put(key, value);
    }
}
