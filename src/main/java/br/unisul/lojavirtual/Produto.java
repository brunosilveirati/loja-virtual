package br.unisul.lojavirtual;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.text.Normalizer;

@Getter
@EqualsAndHashCode
public class Produto {

    private String id;
    private String nome;
    private String descricaoCurta;
    private BigDecimal preco;

    public String getId() {

        if (id == null) {
            id = Normalizer.normalize(nome, Normalizer.Form.NFD)
                           .replaceAll("\\p{M}", "")
                           .toLowerCase()
                           .replace(' ', '-')
                           .trim();
        }
        return id;
    }
}
