package br.unisul.lojavirtual;

public class Bootstrap {

    public static void main(String[] args) throws Exception {
        LojaVirtual lojaVirtual = new LojaVirtual();

        lojaVirtual.inicializar();
    }
}
