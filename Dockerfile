FROM openjdk:8-jre-alpine

RUN adduser -D -u 1000 lojavirtual && mkdir -p /var/opt/lojavirtual/data
WORKDIR /var/opt/lojavirtual
COPY target/*FULL.jar application.jar
VOLUME /var/opt/lojavirtual/data
RUN chown -R lojavirtual:lojavirtual /var/opt/lojavirtual
EXPOSE 7000
USER lojavirtual

CMD java -Xmx256m -jar application.jar